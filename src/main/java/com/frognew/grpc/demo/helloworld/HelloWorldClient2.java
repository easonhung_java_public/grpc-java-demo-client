package com.frognew.grpc.demo.helloworld;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;

public class HelloWorldClient2 {
	private static final Logger LOGGER = LoggerFactory.getLogger(HelloWorldClient2.class);

	private final ManagedChannel channel;
	private final Service2Grpc.Service2BlockingStub blockingStub;

	/** Construct client connecting to HelloWorld server at {@code host:port}. */
	public HelloWorldClient2(String host, int port) {
		this(ManagedChannelBuilder.forAddress(host, port)
				// Channels are secure by default (via SSL/TLS). For the example we disable TLS
				// to avoid
				// needing certificates.
				.usePlaintext(true));
	}

	/**
	 * Construct client for accessing RouteGuide server using the existing channel.
	 */
	HelloWorldClient2(ManagedChannelBuilder<?> channelBuilder) {
		channel = channelBuilder.build();
		blockingStub = Service2Grpc.newBlockingStub(channel);
	}

	public void shutdown() throws InterruptedException {
		channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
	}

	/** Say hello to server. */
	public void test(String name, String age) {
		LOGGER.info("Input name =  " + name + " ...");
		LOGGER.info("Input age =  " + age + " ...");
		TestService2Request request = TestService2Request.newBuilder().setName(name).setAge(age).build();
		TestService2Reply response;
		try {
			response = blockingStub.testService2(request);
		} catch (StatusRuntimeException e) {
			LOGGER.warn("RPC failed: {}", e.getStatus());
			return;
		}
		LOGGER.info("getResponse: " + response.getResponse());
	}

	/**
	 * Greet server. If provided, the first element of {@code args} is the name to
	 * use in the greeting.
	 */
	public static void main(String[] args) throws Exception {
		HelloWorldClient2 client = new HelloWorldClient2("localhost", 50051);
		try {
			client.test("name1","20");
		} finally {
			client.shutdown();
		}
	}
}
