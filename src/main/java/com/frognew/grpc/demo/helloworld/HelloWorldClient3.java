package com.frognew.grpc.demo.helloworld;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;

public class HelloWorldClient3 {
	private static final Logger LOGGER = LoggerFactory.getLogger(HelloWorldClient3.class);

	private final ManagedChannel channel;
	private final Service2Grpc.Service2BlockingStub blockingStub;

	/** Construct client connecting to HelloWorld server at {@code host:port}. */
	public HelloWorldClient3(String host, int port) {
		this(ManagedChannelBuilder.forAddress(host, port)
				// Channels are secure by default (via SSL/TLS). For the example we disable TLS
				// to avoid
				// needing certificates.
				.usePlaintext(true));
	}

	/**
	 * Construct client for accessing RouteGuide server using the existing channel.
	 */
	HelloWorldClient3(ManagedChannelBuilder<?> channelBuilder) {
		channel = channelBuilder.build();
		blockingStub = Service2Grpc.newBlockingStub(channel);
	}

	public void shutdown() throws InterruptedException {
		channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
	}

	/** Say hello to server. */
	public void test(String name, String age) {
		LOGGER.info("Input name =  " + name + " ...");
		LOGGER.info("Input age =  " + age + " ...");
		TestService2Request request = TestService2Request.newBuilder().setName(name).setAge(age).build();
		TestService2Reply response;
		try {
			response = blockingStub.testService2(request);
		} catch (StatusRuntimeException e) {
			LOGGER.warn("RPC failed: {}", e.getStatus());
			return;
		}
		LOGGER.info("getResponse: " + response.getResponse());
	}

	/**
	 * Greet server. If provided, the first element of {@code args} is the name to
	 * use in the greeting.
	 */
	public static void main(String[] args) throws Exception {
		HelloWorldClient3 client = new HelloWorldClient3("localhost", 50052);
		try {
			client.test("name2","30");
		} finally {
			client.shutdown();
		}
	}
}
