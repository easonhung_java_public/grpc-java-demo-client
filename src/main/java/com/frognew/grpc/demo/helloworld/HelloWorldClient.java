package com.frognew.grpc.demo.helloworld;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;

public class HelloWorldClient {
	private static final Logger LOGGER = LoggerFactory.getLogger(HelloWorldClient.class);

	private final ManagedChannel channel;
	private final GreeterGrpc.GreeterBlockingStub blockingStub;

	/** Construct client connecting to HelloWorld server at {@code host:port}. */
	public HelloWorldClient(String host, int port) {
		this(ManagedChannelBuilder.forAddress(host, port)
				// Channels are secure by default (via SSL/TLS). For the example we disable TLS
				// to avoid
				// needing certificates.
				.usePlaintext(true));
	}

	/**
	 * Construct client for accessing RouteGuide server using the existing channel.
	 */
	HelloWorldClient(ManagedChannelBuilder<?> channelBuilder) {
		channel = channelBuilder.build();
		blockingStub = GreeterGrpc.newBlockingStub(channel);
	}

	public void shutdown() throws InterruptedException {
		channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
	}

	/** Say hello to server. */
	public void test(String id, String pwd) {
		LOGGER.info("Input id =  " + id + " ...");
		LOGGER.info("Input pwd =  " + pwd + " ...");
		TestRequest request = TestRequest.newBuilder().setId(id).setPwd(pwd).build();
		TestReply response;
		try {
			response = blockingStub.test(request);
		} catch (StatusRuntimeException e) {
			LOGGER.warn("RPC failed: {}", e.getStatus());
			return;
		}
		LOGGER.info("Greeting: " + response.getResponse());
	}

	/**
	 * Greet server. If provided, the first element of {@code args} is the name to
	 * use in the greeting.
	 */
	public static void main(String[] args) throws Exception {
		HelloWorldClient client = new HelloWorldClient("localhost", 50051);
		try {
			client.test("testid","testpwd");
		} finally {
			client.shutdown();
		}
	}
}
