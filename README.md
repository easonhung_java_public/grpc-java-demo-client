# grpc-java-demo-client

##### openjdk version: 1.8.0_191
##### gradle version: Gradle 5.0

this repository is a workable project, if you want to init a new project, you can follow the steps:

## step 1
```
mkdir grpc-java-demo-client
```

## step 2
```
cd grpc-java-demo-client
gradle init --type=java-application
```

## step 3 modify build.gradle
```
apply plugin: 'java'
apply plugin: 'com.google.protobuf'
apply plugin: 'eclipse'

buildscript {
  repositories {
    mavenCentral()
  }
  dependencies {
    classpath 'com.google.protobuf:protobuf-gradle-plugin:0.8.4'
  }
}

repositories {
   mavenCentral()
}

sourceSets {
   main {
   	java {
   		srcDir 'build/generated/source/proto/main/java'
   		srcDir 'build/generated/source/proto/main/grpc'
   	}
   }
}

compileJava {
    sourceCompatibility=1.8
    targetCompatibility=1.8
    options.encoding='UTF-8'
}
compileTestJava {
    sourceCompatibility=1.8
    targetCompatibility=1.8
    options.encoding='UTF-8'
}

configurations { 
	all*.exclude group:'com.sun.xml.bind',module:'jaxb-impl'
	all*.exclude group:'xml-apis',module:'xml-apis'
	all*.exclude group:'stax',module:'stax-api'
	all*.exclude group:'org.slf4j',module:'slf4j-log4j12'
	all*.exclude group:'commons-logging'
}

ext {
	grpcVersion= '1.3.0'
	logbackVersion = '1.2.2'
	slf4jVersion='1.7.25'
}

dependencies {
    compile "io.grpc:grpc-netty:${grpcVersion}"
    compile "io.grpc:grpc-protobuf:${grpcVersion}"
    compile "io.grpc:grpc-stub:${grpcVersion}"
    
    compile "org.slf4j:slf4j-api:$slf4jVersion"
    compile "ch.qos.logback:logback-classic:$logbackVersion"
    runtime "org.slf4j:jcl-over-slf4j:$slf4jVersion"
    runtime "org.slf4j:log4j-over-slf4j:$slf4jVersion"
    runtime "org.slf4j:jul-to-slf4j:$slf4jVersion"
    testCompile 'junit:junit:4.12'
}

protobuf {
  protoc {
    artifact = 'com.google.protobuf:protoc:3.2.0'
  }
  plugins {
    grpc {
      artifact = "io.grpc:protoc-gen-grpc-java:${grpcVersion}"
    }
  }
  generateProtoTasks {
    all()*.plugins {
      grpc {
        option 'enable_deprecated=false'
      }
    }
  }
}

tasks.eclipse.dependsOn compileJava
```

## step 4 create src/main/resources/logback.xml：
```
<?xml version="1.0" encoding="UTF-8"?>

<configuration debug="false" scan="false" scanPeriod="30 seconds">

  <contextName>grpc-java-demo</contextName>

  <appender name="STDOUT" class="ch.qos.logback.core.ConsoleAppender">
    <encoder>
      <pattern>
        <![CDATA[
        [%date{yyyy-MM-dd HH:mm:ss.SSS}] [%thread] %level %logger - %message%n
        ]]>
      </pattern>
    </encoder>
  </appender>

  <logger name="com.frognew.grpc.demo" level="INFO"  additivity="false">
  	<appender-ref ref="STDOUT" />
  </logger>

  <root level="WARN">
    <appender-ref ref="STDOUT" />
  </root>

</configuration>
```
## step 5 remove src/main/java/App.java and src/test/java/AppTest.java

## step 6 create src/proto/helloworld.proto:
```
syntax = "proto3";

option java_multiple_files = true;
option java_package = "com.frognew.grpc.demo.helloworld";
option java_outer_classname = "HelloWorldProto";
option objc_class_prefix = "HLW";

package helloworld;

service Greeter {
  rpc SayHello (HelloRequest) returns (HelloReply) {}
}

message HelloRequest {
  string name = 1;
}

message HelloReply {
  string message = 1;
}
```

## step 7
```
gradle eclipse
```

## setp 8 create com.frognew.grpc.demo.helloworld.HelloWorldClient.java
```
package com.frognew.grpc.demo.helloworld;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;

public class HelloWorldClient {
	private static final Logger LOGGER = LoggerFactory.getLogger(HelloWorldClient.class);
	
	private final ManagedChannel channel;
	  private final GreeterGrpc.GreeterBlockingStub blockingStub;

	  /** Construct client connecting to HelloWorld server at {@code host:port}. */
	  public HelloWorldClient(String host, int port) {
	    this(ManagedChannelBuilder.forAddress(host, port)
	        // Channels are secure by default (via SSL/TLS). For the example we disable TLS to avoid
	        // needing certificates.
	        .usePlaintext(true));
	  }

	  /** Construct client for accessing RouteGuide server using the existing channel. */
	  HelloWorldClient(ManagedChannelBuilder<?> channelBuilder) {
	    channel = channelBuilder.build();
	    blockingStub = GreeterGrpc.newBlockingStub(channel);
	  }

	  public void shutdown() throws InterruptedException {
	    channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
	  }

	  /** Say hello to server. */
	  public void greet(String name) {
	    LOGGER.info("Will try to greet " + name + " ...");
	    HelloRequest request = HelloRequest.newBuilder().setName(name).build();
	    HelloReply response;
	    try {
	      response = blockingStub.sayHello(request);
	    } catch (StatusRuntimeException e) {
	      LOGGER.warn("RPC failed: {}", e.getStatus());
	      return;
	    }
	    LOGGER.info("Greeting: " + response.getMessage());
	  }

	  /**
	   * Greet server. If provided, the first element of {@code args} is the name to use in the
	   * greeting.
	   */
	  public static void main(String[] args) throws Exception {
	    HelloWorldClient client = new HelloWorldClient("localhost", 50051);
	    try {
	      /* Access a service running on the local machine on port 50051 */
	      String user = "world";
	      if (args.length > 0) {
	        user = args[0]; /* Use the arg as the name to greet if provided */
	      }
	      client.greet(user);
	    } finally {
	      client.shutdown();
	    }
	  }
}
```

## step 9 run as java application
```
Will try to greet world ...
Greeting: Hello world
```

if you see this ouput, congratulations!


refer to https://blog.frognew.com/2017/05/grpc-java-quick-start.html#helloworldserver%E5%92%8Chelloworldclient